var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');


var socket_states = {
			        	CONNECTED : { state_code: 0 , state : "CONNECTED" },
          				WAITING_IN_QUEUE : { state_code: 1 , state : "WAITING_IN_QUEUE" },
          				IN_HANDSHAKE : { state_code: 2, state : "IN_HANDSHAKE" },
          				PAIRED : { state_code: 3 , state : "PAIRED" },
          				DISCONNECTED : { state_code: 4 , state : "DISCONNECTED" },
						PARTNER_DISCONNECTED : { state_code: 5 , state : "PARTNER HAS DISCONNECTED"} 
      				};

var GLOBAL_USER_COUNT = 0 ;



var socketsQueue = [];
var rooms =  [];

// Routing the request for index using express
app.get('/',function(req,res){

	res.sendFile(__dirname + '/view/index.html');

});

// Working with Socket.io //
io.on('connection',function(socket){
	
	
	GLOBAL_USER_COUNT++;
	
	

	// Immediately notifying others that user has joined
	socket.emit('user_joined',
									{
										message_text:'User joined!',
										global_user_count:GLOBAL_USER_COUNT
									});

	//when socket is ready to put up in queue
	socket.on('WAITING_IN_QUEUE',function  (msg) {

		socketsQueue.push(socket);

		if(socketsQueue.length >= 2){			


			var socket_1 = socketsQueue.shift();
			var socket_2 = socketsQueue.shift();


			socket.broadcast.to(socket_1.id).emit('IN_HANDSHAKE',{global_user_count:GLOBAL_USER_COUNT});
			socket.broadcast.to(socket_2.id).emit('IN_HANDSHAKE',{global_user_count:GLOBAL_USER_COUNT});

			var roomID = socket_1.id + socket_2.id ;
			
			rooms[socket_1.id] = roomID;
			rooms[socket_2.id] = roomID;


       		socket.broadcast.to(socket_1.id).emit('PAIRED', {global_user_count:GLOBAL_USER_COUNT});

       		socket.broadcast.to(socket_2.id).emit('PAIRED', {global_user_count:GLOBAL_USER_COUNT});


       		socket_1.join(roomID);
       		socket_2.join(roomID);

		}
		
	});



	// when a socket sends a  broadcast message with 'chat_message'
	socket.on('msg',function (msg) {
		if(rooms[socket.id]!==undefined){
			 io.sockets.in(rooms[socket.id]).emit('msg', { message: msg ,global_user_count :GLOBAL_USER_COUNT});
		}
	});
	
	socket.on('user_typing',function(msg){
          //$('#messages').append($('<li class="disconnected">').text(msg.message_text));
          if(rooms[socket.id]!==undefined){
			 io.sockets.in(rooms[socket.id]).emit('user_typing', { message: msg ,global_user_count :GLOBAL_USER_COUNT});
		}
	});

	socket.on('user_stopped_typing',function(msg){
          //$('#messages').append($('<li class="disconnected">').text(msg.message_text));
          if(rooms[socket.id]!==undefined){
			 io.sockets.in(rooms[socket.id]).emit('user_stopped_typing', { message: msg ,global_user_count :GLOBAL_USER_COUNT});
		}

      });

	// When the socket disconnects
	socket.on('disconnect',function () {
		
		console.log('socket disconnected');
		GLOBAL_USER_COUNT--;

		socket.emit('user_left',
											{
												message_text:'User Disconnected!',
												global_user_count:GLOBAL_USER_COUNT
											});
		io.sockets.in(rooms[socket.id]).emit('PARTNER_DISCONNECTED', { message: socket_states.PARTNER_DISCONNECTED.state,global_user_count:GLOBAL_USER_COUNT});
	});




});

http.listen(3000,function(){
	console.log('Server active');
})